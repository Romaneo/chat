﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using ChatAlfa01.Helpers;
using ChatAlfa01.Models;
using ChatAlfa01.Models.Chat;
using ChatAlfa01.Services;
using ChatAlfa01.ViewModels.Chat;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.Data.Entity;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ChatAlfa01.Controllers
{
    [Microsoft.AspNet.Authorization.Authorize]
    public class BranchController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IHubContext _testHub;

        public BranchController(ApplicationDbContext context, IConnectionManager connectionManager)
        {
            _context = context;
            _testHub = connectionManager.GetHubContext<ChatHub>();
        }

        public IActionResult GetMAinBranch(int convId)
        {
            var user = _context.Users.Include(x => x.Author).Include(x => x.Conversations).First(x => x.UserName == User.GetUserName());
            var conversation = _context.Conversations.Include(x => x.Members)
                    .Include(x => x.Branches)
                    .FirstOrDefault(x => x.Id == convId);
            var branch = conversation?.Branches.First(x => x.IsMain).ToViewModel();
            if (branch == null)
            {
                return HttpNotFound();
            }
            return Json(branch);
        }

        public IActionResult GetBranches(int id)
        {
            var user = _context.Users.Include(x => x.Author).Include(x => x.Conversations).First(x => x.UserName == User.GetUserName());
            var conversation = _context.Conversations.Include(x => x.Members)
                    .Include(x => x.Branches)
                    .FirstOrDefault(x => x.Id == id);
            var branches = conversation?.Branches.Select(x => x.ToViewModel()).ToList();
            if (branches == null)
            {
                return HttpNotFound();
            }
            return Json(branches);
        }

        public IActionResult GetBranch(int id)
        {
            var branch = _context.Branches.
                Include(x => x.Conversation).
                Include(x => x.Members).ThenInclude(x => x.Member)
                .First(x => x.Id == id).ToViewModel();
            return Json(branch);
        }

        [HttpPost]
        public IActionResult EditBranch([FromBody] BranchViewModel branch)
        {
            var br = _context.Branches
                .Include(x => x.Members).ThenInclude(x => x.Member)
                .First(x => x.Id == branch.Id);
            if (branch.Title != br.Name)
            {
                br.Name = branch.Title;
            }

            foreach (var member in branch.Members)
            {
                if (!br.Members.Any(x => x.Member.UserName == member))
                {
                    br.Members.Add(new ConversationMember
                    {
                        Member = _context.Users.First(x => x.UserName == member),
                        Conversation = br.Conversation,
                        Branch = br
                    });
                }
            }

            foreach (var member in br.Members)
            {
                if (!branch.Members.Contains(member.Member.UserName))
                {
                    _context.ConversationMembers.Remove(member);
                }
            }
            _context.SaveChanges();
            _testHub.Clients.All.updateTree();
            return Ok();
        }

        public IActionResult AddBranch([FromBody] BranchViewModel branch)
        {
            var user = _context.Users.First(x => x.UserName == User.GetUserName());
            var conv = _context.Conversations.First(x => x.Id == branch.ConvId);
            string path = "/";
            int level = 1;
            if (branch.Id != 0)
            {
                var t = _context.Branches.First(x => x.Id == branch.Id);
                path = $"{t.Path}/{branch.Title}";
                level = _context.Branches.First(x => x.Id == branch.Id).Level + 1;
            }
            else
            {
                branch.Id =
                    _context.Conversations.Include(x => x.Branches)
                        .First(x => x.Id == branch.ConvId)
                        .Branches.First()
                        .Id;
            }
            if (level > 42)
            {
                return new BadRequestResult();
            }




            //var members = branch.Members.Select(member => _context.Users.First(x => x.UserName == member)).ToList();
            var b = new Branch
            {
                Name = branch.Title,
                ParentId = branch.Id,
                Conversation = _context.Conversations.First(x => x.Id == branch.ConvId),
                Path = path,
                Level = level,
                IsMain = false,
            };

            var cml = new List<ConversationMember>();
            branch.Members.Add(user.UserName);
            foreach (var name in branch.Members)
            {
                var member = _context.Users.First(x => x.UserName == name);
                cml.Add(new ConversationMember
                {
                    Conversation = conv,
                    Branch = b,
                    Member = member,
                    Notify = true
                });
            }
            _context.Branches.Add(b);
            _context.ConversationMembers.AddRange(cml);
            _context.SaveChanges();
            _testHub.Clients.All.updateTree();
            return Ok();
        }
    }
}
