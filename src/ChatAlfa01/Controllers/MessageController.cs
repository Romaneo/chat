﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using ChatAlfa01.Helpers;
using ChatAlfa01.Models;
using ChatAlfa01.Services;
using ChatAlfa01.ViewModels.Chat;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.Data.Entity;

namespace ChatAlfa01.Controllers
{
    [Microsoft.AspNet.Authorization.Authorize]
    public class MessageController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IHubContext testHub;
        public MessageController(ApplicationDbContext context, IConnectionManager connectionManager)
        {
            _context = context;
            testHub = connectionManager.GetHubContext<ChatHub>();
        }

        [HttpPost]
        public IActionResult AddMessage([FromBody]MessageViewModel message)
        {
            if (message.BranchId == 0)
            {
                var conversation = _context.Conversations
                    .Include(x => x.Members).
                    Include(x => x.Members).ThenInclude(x => x.Member).
                    Include(x => x.Branches)
                    //.Where(x => x.Creator.UserName == User.GetUserName())
                    .First(x => x.Id == message.ConversationId);
                message.BranchId = conversation.Branches.First(x => x.IsMain).Id;
            }
            var user = _context.Users.First(x => x.UserName == User.GetUserName());

            var t = message.ToModel(_context, user);
            _context.Messages.Add(t);
            _context.SaveChanges();

            testHub.Clients.All.receiveHello(t.ToViewModel());

            return Ok();
        }

        public IActionResult GetMessages(int convId, int? branchId = null)
        {
            var user = _context.Users.First(x => x.UserName == User.GetUserName());
            
            var conversation = _context.Conversations
                .Include(x => x.Branches).ThenInclude(x => x.Messages)
                .Include(x => x.Members).ThenInclude(x=>x.Member)
                .FirstOrDefault(x => x.Id == convId);
            if (conversation == null)
            {
                Response.StatusCode = HttpBadRequest().StatusCode;
                return Json("Conversation not found");
            }
            
            var branch = branchId == null
                ? conversation.Branches.First(x => x.IsMain)
                : conversation.Branches.FirstOrDefault(x => x.Id == branchId);
            var t =_context.ConversationMembers.Any(x => x.Conversation.Id == convId 
                    && x.Branch.Id == branch.Id 
                    && x.MemberId == user.Id);
            if (!t)
            {
                return new BadRequestResult();
            }
            List<MessageViewModel> messages = new List<MessageViewModel>();
            try
            {
                messages = branch.Messages.Where(x => x.Time > user.LogOutTime).OrderBy(x => x.Time).Select(x => x.ToViewModel()).ToList();
            }
            catch (Exception e)
            {
                var r = e.Message;
            }
            return Json(messages);
        }
    }
}
