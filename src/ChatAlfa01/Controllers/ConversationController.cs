﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using ChatAlfa01.Helpers;
using ChatAlfa01.Models;
using ChatAlfa01.Models.Chat;
using ChatAlfa01.Services;
using ChatAlfa01.ViewModels.Chat;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.Data.Entity;

namespace ChatAlfa01.Controllers
{
    [Microsoft.AspNet.Authorization.Authorize]
    public class ConversationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IHubContext _testHub;

        public ConversationController(ApplicationDbContext context, IConnectionManager connectionManager)
        {
            _context = context;
            _testHub = connectionManager.GetHubContext<ChatHub>();
        }

        public IActionResult GetConversationTree()
        {
            var user = _context.Users.Include(x => x.Author).Include(x => x.Conversations).First(x => x.UserName == User.GetUserName());
            List<Conversation> conversations = new List<Conversation>();
            try
            {
                var t = _context.ConversationMembers.Where(x => x.MemberId == user.Id).Select(x => x.Conversation.Id).ToList();
                conversations = _context.Conversations
                .Include(x => x.Members).
                Include(x => x.Members).ThenInclude(x => x.Member).
                Include(x => x.Branches).Where(x => t.Contains(x.Id)).ToList();
            }
            catch (Exception e)
            {
                var t = e.Message;
            }
            var ctvl = conversations.Select(conv => new ConversationTreeItem
            {
                Id = conv.Id,
                Name = conv.Title,
                Branches = SelectBranchesFromConversation(conv)
            }).ToList();

            return Json(ctvl);
        }

        private List<BranchTreeItem> SelectBranchesFromConversation(Conversation conv)
        {
            var res = new List<BranchTreeItem>();

            var maxLevel = conv.Branches.Max(x => x.Level);
            var roots = new List<BranchTreeItem>();

            for (var level = maxLevel; level > 0; level--)
            {
                var lastBranches = conv.Branches.Where(x => x.Level == level).ToList();
                var parents = new List<BranchTreeItem>();

                foreach (var branch in lastBranches)
                {
                    if (branch.ParentId == 0)
                    {
                        if (roots.All(x => x.Id != branch.Id))
                        {
                            parents.Add(new BranchTreeItem
                            {
                                Id = branch.Id,
                                Name = branch.Name,
                                ConvId = conv.Id
                            });
                        }

                        continue;
                    }
                    var parent = conv.Branches.First(x => x.Id == branch.ParentId);
                    if (parents.All(x => x.Id != parent.Id))
                    {
                        parents.Add(new BranchTreeItem
                        {
                            Id = parent.Id,
                            Name = parent.Name,
                            ConvId = conv.Id
                        });
                    }

                    var childBranch = roots.Any(x => x.Id == branch.Id)
                        ? roots.First(x => x.Id == branch.Id)
                        : new BranchTreeItem
                        {
                            Id = branch.Id,
                            Name = branch.Name,
                            ConvId = conv.Id
                        };

                    parents.First(x => x.Id == parent.Id).Branches.Add(childBranch);
                }
                roots.Clear();
                roots.AddRange(parents);
            }
            //return Json(roots);
            return roots;
        }

        [HttpPost]
        public IActionResult EditConversation([FromBody] ConversationViewModel conversation)
        {
            var conv = _context.Conversations
                .Include(x => x.Members).
                Include(x => x.Members).ThenInclude(x => x.Member).
                Include(x => x.Branches).ToList().First(x => x.Id == conversation.Id);
            if (conv.Title != conversation.Title)
            {
                conv.Title = conversation.Title;
            }

            foreach (var member in conversation.Members)
            {
                if (!conv.Members.Any(x => x.Member.UserName == member))
                {
                    conv.Members.Add(new ConversationMember
                    {
                        Member = _context.Users.First(x => x.UserName == member),
                        Conversation = conv,
                        Branch = conv.Branches.First(x => x.IsMain)
                    });
                }
            }

            foreach (var member in conv.Members)
            {
                if (!conversation.Members.Contains(member.Member.UserName))
                {
                    _context.ConversationMembers.Remove(member);
                }
            }
            _context.SaveChanges();
            _testHub.Clients.All.updateTree();
            return Ok();
        }

        public IActionResult GetConversation(int id)
        {
            var user = _context.Users.Include(x => x.Author).Include(x => x.Conversations).First(x => x.UserName == User.GetUserName());
            var conversations = _context.Conversations
                .Include(x => x.Members).
                Include(x => x.Members).ThenInclude(x => x.Member).
                Include(x => x.Branches).ToList();
            var conversationvms = conversations.First(x => x.Id == id).ToViewModel();
            return Json(conversationvms);
        }

        [HttpPost]
        public IActionResult AddConversation([FromBody]ConversationViewModel conversation)
        {
            var user = _context.Users.First(x => x.UserName == User.GetUserName());
            var conversation1 = new Conversation()
            {
                CreatorId = user.Id,
                Title = conversation.Title,
                IsPublic = conversation.IsPublic,

            };
            var branch = new Branch
            {
                IsMain = true,
                ParentId = 0,
                Level = 0,
                Path = "/",
                Name = "Main"
            };

            var cml = new List<ConversationMember>();
            conversation.Members.Add(user.UserName);
            foreach (var name in conversation.Members)
            {
                var member = _context.Users.First(x => x.UserName == name);
                cml.Add(new ConversationMember
                {
                    Conversation = conversation1,
                    Branch = branch,
                    Member = member,
                    Notify = true
                });
            }


            conversation1.Branches.Add(branch);
            _context.Conversations.Add(conversation1);
            _context.ConversationMembers.AddRange(cml);
            _context.SaveChanges();
            _testHub.Clients.All.updateTree();
            return Ok("New conversation added.");
        }

    }
}
