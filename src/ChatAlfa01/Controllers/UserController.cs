﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using ChatAlfa01.Helpers;
using ChatAlfa01.Models;
using ChatAlfa01.Models.Chat;
using ChatAlfa01.ViewModels.Chat;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ChatAlfa01.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UserController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Search(string name)
        {
            if (name.Length < 2)
            {
                return new BadRequestResult();
            }
            var users = _context.Users.Where(x => x.UserName.Contains(name))
                .OrderBy(x => x.UserName.IndexOf(name, StringComparison.Ordinal))
                .Select(x => x.ToViewModel());
            return Json(users);
        }

        public JsonResult GetUser(string name)
        {
            var user = _context.Users.Include(x => x.Author).Include(x => x.Conversations).First(x => x.UserName == name).ToViewModel();
            return Json(user);
        }

        public IActionResult GetMembers(int convId,int? branchId)
        {
            //var conversation = _context.Conversations
            //        .Include(x => x.Members).
            //        Include(x => x.Members).ThenInclude(x => x.Member).
            //        Include(x => x.Branches)
            //        .First(x => x.Id == convId);
            //var t = conversation.ToViewModel();
            //return Json(t.Members.Distinct().ToList());

            var ce = _context.Conversations.Any(x => x.Id == convId);
            var be = branchId ?? _context.Conversations.Include(x=>x.Branches).First(x => x.Id == convId).Branches.First(x => x.IsMain).Id;
            var t = _context.ConversationMembers
                .Include(x => x.Member)
                .Include(x => x.Conversation)
                .Include(x => x.Branch)
                .Where(x => x.Conversation.Id == convId && x.Branch.Id == be)
                .Select(x => x.Member.UserName).ToList().Distinct().ToList();
            return Json(t);
        }
    }
}
