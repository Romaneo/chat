﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatAlfa01.Models;
using ChatAlfa01.Models.Chat;
using ChatAlfa01.ViewModels.Chat;

namespace ChatAlfa01.Helpers
{
    public static class Mapper
    {
        //public static BranchTreeItem ToViewModel(this Branch b )
        //{
        //    return new BranchTreeItem
        //    {
        //        Id = b.Id,
        //        Name = b.Name,

        //    };
        //}

        public static ConversationViewModel ToViewModel(this Conversation conversation)
        {
            var t = conversation.Members?.Select(x => x.Member.UserName).ToList();
            return new ConversationViewModel
            {
                Id = conversation.Id,
                Title = conversation.Title,
                CreatorId = conversation.CreatorId,
                IsPublic = conversation.IsPublic,
                BranchIds = conversation.Branches?.Select(x => x.Id).ToList(),
                Members = t
            };
        }

        public static ConversationMemberViewModel ToViewModel(this ConversationMember conversationMember)
        {
            return new ConversationMemberViewModel
            {
                Id = conversationMember.Id,
                BranchId = conversationMember.Branch.Id,
                ConversationId = conversationMember.Conversation.Id,
                Notify = conversationMember.Notify,
                UserId = conversationMember.Member.Id
            };
        }

        public static MessageViewModel ToViewModel(this Message m)
        {
            var y = m;
            return new MessageViewModel
            {
                Id = m.Id,
                Time = m.Time,
                BranchId = m.Branch.Id,
                Author = m.Author.UserName,
                Text = m.Text,
                HasAttacments = m.HasAttacments,
                AttachmentIds = m.HasAttacments ? m.Attachments.Select(x => x.Id).ToList() : new List<int>(),
                UserIcon = @"/images/user.png"

            };
        }

        public static BranchViewModel ToViewModel(this Branch b)
        {
            var t = b.Members?.Select(x => x.Member.UserName).ToList();
            return new BranchViewModel
            {
                Id = b.Id,
                IsMain = b.IsMain,
                Title = b.Name,
                Level = b.Level,
                ParentId = b.ParentId,
                Path = b.Path,
                MessagesId = b.Messages?.Select(x => x.Id).ToList(),
                ConvId = b.Conversation.Id,
                Members = t
            };
        }

        public static AttachmentViewModel ToViewModel(this Attachment a)
        {
            return new AttachmentViewModel
            {
                Id = a.Id,
                Name = a.Name,
                FilePath = a.FilePath,
                Hash = a.Hash,
                MessageId = a.Message.Id
            };
        }

        public static UserViewModel ToViewModel(this ApplicationUser u)
        {
            return new UserViewModel
            {
                Name = u.UserName,
                Id = u.Id,
            };
        }

        public static Message ToModel(this MessageViewModel m, ApplicationDbContext c, ApplicationUser u)
        {
            return new Message
            {
                Author = u,
                Branch = c.Branches.First(x => x.Id == m.BranchId),
                Text = m.Text,
                Time = DateTime.Now
            };
        }
    }
}
