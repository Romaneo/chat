﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAlfa01.Services
{
    public class User
    {
        public string ConnectionId { get; set; }
        public string Name { get; set; }
    }

    public class Subscribe
    {
        public int Id { get; set; }
        public int Count { get; set; }
        public bool IsConv { get; set; }
    }
}
