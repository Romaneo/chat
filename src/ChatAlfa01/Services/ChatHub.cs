﻿using System.Collections.Generic;
using System.Linq;
using ChatAlfa01.Models;
using ChatAlfa01.ViewModels.Chat;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace ChatAlfa01.Services
{
    [HubName("chatHub")]
    public class ChatHub : Hub
    {
        private readonly ApplicationDbContext _context;
        static List<User> Users = new List<User>();

        public ChatHub(ApplicationDbContext context)
        {
            _context = context;
        }

        public void broadcastHello([FromBody]MessageViewModel message)
        {
            Clients.All.receiveHello(message);
        }

        // Отправка сообщений
        public void Send(string name, string message)
        {
            message += _context.Messages.First().Text;
            Clients.All.addMessage(name, message);
            //Clients.Group("").addMessage(name, message);
        }

        // Подключение нового пользователя
        public void Connect(string userName)
        {
            var id = Context.ConnectionId;


            if (Users.All(x => x.ConnectionId != id))
            {
                Users.Add(new User { ConnectionId = id, Name = userName });

                // Посылаем сообщение текущему пользователю
                Clients.Caller.onConnected(id, userName, Users);

                // Посылаем сообщение всем пользователям, кроме текущего
                Clients.AllExcept(id).onNewUserConnected(id, userName);
            }
        }

        // Отключение пользователя
        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Users.Remove(item);
                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(id, item.Name);
            }

            return base.OnDisconnected(stopCalled);
        }
    }
}
