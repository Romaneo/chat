﻿namespace ChatAlfa01.Models.Chat
{
    public class Attachment
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public string Hash { get; set; }
        public string Name { get; set; }

        public virtual Message Message { get; set; }
    }
}
