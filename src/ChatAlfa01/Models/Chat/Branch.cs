﻿using System.Collections.Generic;

namespace ChatAlfa01.Models.Chat
{
    public class Branch
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsMain { get; set; }

        public int ParentId { get; set; }
        public string Path { get; set; }
        public int Level { get; set; }

        public virtual Conversation Conversation { get; set; }
        public virtual ICollection<ConversationMember> Members { get; set; }
        public virtual ICollection<Message> Messages { get; set; }

    }
}
