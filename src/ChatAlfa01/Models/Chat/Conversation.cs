﻿using System.Collections.Generic;

namespace ChatAlfa01.Models.Chat
{
    public class Conversation
    {
        public Conversation()
        {
            Members = new List<ConversationMember>();
            Branches = new List<Branch>();

        }

        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsPublic { get; set; }

        public string CreatorId { get; set; }
        public virtual ApplicationUser Creator { get; set; }
        public virtual ICollection<ConversationMember> Members { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
    }
}
