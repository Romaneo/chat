﻿using System;
using System.Collections.Generic;

namespace ChatAlfa01.Models.Chat
{
    public class Message
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public bool HasAttacments { get; set; }

        public virtual ApplicationUser Author { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
    }
}
