﻿namespace ChatAlfa01.Models.Chat
{
    public class Friend
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
