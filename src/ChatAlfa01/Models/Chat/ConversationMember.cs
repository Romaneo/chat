﻿namespace ChatAlfa01.Models.Chat
{
    public class ConversationMember
    {
        public int Id { get; set; }
        public bool Notify { get; set; }
        public string MemberId { get; set; }

        public virtual ApplicationUser Member { get; set; }
        public virtual Conversation Conversation { get; set; }
        public virtual Branch Branch { get; set; }
    }
}
