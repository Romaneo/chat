﻿using System;
using System.Collections.Generic;
using ChatAlfa01.Models.Chat;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ChatAlfa01.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string IconPath { get; set; }
        public DateTime LogOutTime { get; set; }


        public ICollection<Friend> Friends { get; set; }
        public ICollection<ConversationMember> Conversations { get; set; }
        public ICollection<Conversation> Author { get; set; }

    }
}
