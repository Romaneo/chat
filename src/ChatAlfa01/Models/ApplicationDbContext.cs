﻿using ChatAlfa01.Models.Chat;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace ChatAlfa01.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
        {
            //this.Users.Include(x => x.Author).Include(x => x.Conversations);
            //this.Conversations.Include(x => x.Branches).Include(x => x.Members);
            //this.Branches.Include(x => x.Conversation).Include(x => x.Members).Include(x => x.Messages);
            //this.Messages.Include(x => x.Attachments).Include(x => x.Author).Include(x => x.Branch);
        }

        public DbSet<Friend> Friends { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Conversation> Conversations { get; set; }
        public DbSet<ConversationMember> ConversationMembers { get; set; }
        public DbSet<Message> Messages { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
