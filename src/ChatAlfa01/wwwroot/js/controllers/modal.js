﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('modal', modal);

    modal.$inject = ['$scope', '$uibModalInstance', '$log', '$http', 'conversation'];

    function modal($scope, $uibModalInstance, $log, $http, conversation) {

        $scope.selected = {};
        //$scope.showSelected = function (sel) {
        //    $scope.selectedNode = sel;
        //};

        $scope.showSelected = function (sel) {
            $scope.selected = sel.Name;
            alert(node.Name);

        };

        $scope.conversation = conversation;

        $scope.buttonAdd = true;

        $scope.delete = function (item) {
            var index = $scope.conversation.Members.indexOf(item);
            if (index > -1) {
                $scope.conversation.Members.splice(index, 1);
            }
        }

        $scope.onSelect = function ($item, $model, $label) {
            $scope.user = $item;
            $scope.buttonAdd = false;
        };

        $scope.Add = function () {
            $scope.conversation.Members.push($scope.user);
            $scope.asyncSelected = '';
            $scope.buttonAdd = true;
        }

        $scope.getLocation = function (val) {
            $scope.buttonAdd = true;
            return $http.get('User/Search', {
                params: {
                    name: val
                }
            }).then(function (response) {
                return response.data.map(function (item) {
                    return item.Name;
                });
            });
        };

        $scope.submit = function () {
            $uibModalInstance.close($scope.conversation);
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
