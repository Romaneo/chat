﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('talk', talk);

    talk.$inject = ['api', '$routeParams', '$scope', 'storage', '$http', '$location', 'signal', '$rootScope'];

    function talk(api, $routeParams, $scope, storage, $http, $location, signal, $rootScope) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'talk';
        $scope.data = storage;
        $rootScope.$on('myEvent', function(event, mass) {
            $scope.$apply();
        });

        
        vm.conversationId = $routeParams.id;
        vm.branchId = $routeParams.branchId;

        api.convId = vm.conversationId;
        storage.conversationId = vm.conversationId;
        api.branchId = vm.branchId;



        getMessages();

        getUsers();
        function getUsers() {
            var branch = '';

            if (api.branchId != null) {
                branch = '&branchid=' + api.branchId;
            }
            return $http.get('User/GetMembers?convId=' + api.convId + branch).then(function (response) {
                storage.users = response.data;
                console.log('users', storage.users);
            });
        }

        $scope.$emit('someEvent', vm.branches);



        vm.newMessage = '';
        vm.Send = send;

        function send() {
            var message = {};
            
            message.Text = vm.newMessage;
            message.BranchId = api.branchId;
            message.ConversationId = api.convId;

            //vm.messages.push(message);
            api.sendMessage(message);
            //proxy.server.broadcastHello(message);
            vm.message = {};
            vm.newMessage = '';

        }
        
        function getMessages() {
            api.getMessages()
               .then(function (response) {
                   $scope.data.messages = response.data;
               }, function (error) {
                   console.log('Unable to load customer data: ' + error.message);
                   $location.url('/404');
               });
        }
    }
})();
