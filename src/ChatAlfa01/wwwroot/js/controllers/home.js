﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('home', home);

    home.$inject = ['api', '$routeParams', '$scope', 'storage', '$uibModal', '$log', '$http','$rootScope','signal'];

    function home(api, $routeParams, $scope, storage, $uibModal, $log, $http, $rootScope, signal) {
        var vm = this;
        vm.title = 'home';
        vm.conversation = {
            Title: '',
            Members: [],
            IsPublic: true,
            Window:''
        };

        vm.newBranch = {
            Window:'',
            Title: '',
            ConvId: 0,
            Id: 0,
            Members: []// parent branch
        }
        $rootScope.$on('tree', function (event, mass) {
            getConversationTree();
            $scope.$apply();
        });

        vm.data = storage;

        $scope.treeOptions = {
            nodeChildren: "Branches",
            dirSelectable: true,
            injectClasses: {
                ul: " list-unstyled list scrollable style-1",
                labelSelected: "sel"
            }
        }

        $scope.data = [];

        vm.Edit = edit;
        vm.AddBranch = addBranch;

        vm.CreateConversation = createConversation;
        $scope.hover = hover;
        $scope.settings = settings;


        getConversationTree();


        function addBranch(node) {
            if (node.IsConv) {
                vm.newBranch.ConvId = node.Id;
            } else {
                vm.newBranch.ConvId = node.ConvId;
                vm.newBranch.Id = node.Id;
            }


            var t = $uibModal.open({
                templateUrl: 'views/inviteModalContent.html',
                backdrop: true,
                windowClass: 'modal',
                controller: 'modal',
                resolve: {
                    conversation: function () {
                        return vm.newBranch;
                    }
                }
            });

            t.result.then(function (branch) {
                api.addBranch(branch);
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });

        }

        function createConversation() {
            var t = $uibModal.open({
                templateUrl: 'views/createConversation.html',
                backdrop: true,
                windowClass: 'modal',
                controller: 'modal',
                resolve: {
                    conversation: function () {
                        return vm.conversation;
                    }
                }
            });

            t.result.then(function (conversation) {

                api.createConversation(conversation);
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });

        }

        function getConversation(id) {
            api.getConversation(id)
                .then(function (response) {
                    vm.conversation = response.data;
                }, function (error) {
                    console.log('Unable to load customer data: ' + error.message);
                });
        }

        function hover(node) {
            return node.Show = !node.Show;
        };

        function getConversationTree() {
            api.conversationTree()
               .then(function (response) {
                   $scope.data = response.data;
               }, function (error) {
                   console.log('Unable to load customer data: ' + error.message);
               });
        }

        function settings(node) {
            if (node.IsConv === true) {
                getConversation(node.Id);
                vm.conversation.Window = 'Edit Conversation';
            } else {
                getBranch(node.Id);
                vm.conversation.Window = 'Edit Branch';
            }
            
        }

        function getBranch(id) {
            api.getBranch(id)
                .then(function (response) {
                    vm.conversation = response.data;
                }, function (error) {
                    console.log('Unable to load customer data: ' + error.message);
                });
        }

        function showEditModal() {
            return $uibModal.open({
                templateUrl: 'views/inviteModalContent.html',
                backdrop: true,
                windowClass: 'modal',
                controller: 'modal',
                resolve: {
                    conversation: function () {
                        return vm.conversation;
                    }
                }
            });
        }

        function edit(node) {
            var t = showEditModal();

            if (node.IsConv) {
                t.result.then(function (conversation) {
                    api.editConversation(conversation);
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
            } else {
                t.result.then(function (conversation) {
                    api.editBranch(conversation);
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
            }


        }
    }
})();
