﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('index', index);

    index.$inject = ['$location', '$scope', '$uibModal', '$log', '$http', 'api']; 

    function index($location, $scope, $uibModal, $log, $http, api) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'index';

        //getConversationTree();
        //function getConversationTree() {
        //    api.conversationTree()
        //       .then(function (response) {
        //           $scope.data = response.data;
        //       }, function (error) {
        //           console.log('Unable to load customer data: ' + error.message);
        //       });
        //}

        $scope.data = [];

        $scope.treeOptions = {
            nodeChildren: "Branches",
            dirSelectable: true,
            injectClasses: {
                ul: "a1",
                li: "a2",
                liSelected: "a7",
                iExpanded: "a3",
                iCollapsed: "a4",
                iLeaf: "a5",
                label: "a6",
                labelSelected: "a8"
            }
        }

        //$scope.show = function() {
        //    alert($scope.conversation.Title);
        //}

        //$scope.open = function () {

        //    $uibModal.open({
        //        templateUrl: 'views/myModalContent.html',
        //        backdrop: true,
        //        windowClass: 'modal',
        //        controller: function ($scope, $uibModalInstance, $log, conversation) {
        //            $scope.conversation = conversation;
        //            $scope.submit = function () {
        //                $log.log('Submiting user info.');
        //                $log.log(conversation);
        //                $uibModalInstance.dismiss('cancel');
        //            }
        //            $scope.cancel = function () {
        //                $uibModalInstance.dismiss('cancel');
        //            };
        //        },
        //        resolve: {
        //            conversation: function () {
        //                return $scope.conversation;
        //            }
        //        }
        //    });            
        //};
    }
})();
