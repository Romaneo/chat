﻿(function () {
    'use strict';

    angular.module('app', [
        'ngRoute',
        'ui.bootstrap',
        'treeControl'
    ]).config(function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'views/index.html',
            controller: 'index',
            controllerAs: 'vm'
        });
        $routeProvider.when("/talk/:id/:branchId?", {
            templateUrl: "views/talk.html",
            controller: 'talk',
            controllerAs: 'vm'
        });
        $routeProvider.when('/404', {
            templateUrl: 'views/404.html'
            //controller: 'Four04Controller'
        });
        $routeProvider.otherwise({ redirectTo: '/' });
    });
})();