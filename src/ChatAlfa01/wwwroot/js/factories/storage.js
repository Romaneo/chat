﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('storage', storage);

    storage.$inject = ['$http'];

    function storage($http) {
        var conversations, branches, convId, users, messages;
        var service = {
            conversations: conversations,
            branches: branches,
            conversationId: convId,
            users: users,
            messages: messages

        };

        return service;

    }
})();