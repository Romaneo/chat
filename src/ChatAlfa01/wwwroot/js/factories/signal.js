﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('signal', signal);

    signal.$inject = ['$rootScope', 'storage'];

    function signal($rootScope, storage) {
        var proxy = $.connection.chatHub;


        proxy.client.receiveHello = function (message) {
            storage.messages.push(message);
            $rootScope.$emit("myEvent", {});
        }

        proxy.client.updateTree = function () {
            $rootScope.$emit("tree", {});
        }

        $.connection.hub.start().done(function () {
            console.log('Hub Connected');
        });



        return proxy;
        //return 'aa';
    }
})();