﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('api', api);

    api.$inject = ['$http'];

    function api($http) {
        var convId, branchId;


        var service = {
            convId: convId,
            branchId: branchId,

            getConversation: getConversation,
            getBranches: getBranches,
            getMessages: getMessages,
            sendMessage: sendMessage,
            createConversation: createConversation,
            conversationTree: getConversationTree,
            editConversation: editConversation,
            getBranch: getBranch,
            editBranch: editBranch,
            addBranch: addBranch

        };

        return service;

        function getConversationTree() {
            return $http.get('/Conversation/GetConversationTree');
        }

        function getConversation(id) {
            return $http.get('/Conversation/GetConversation?id=' + id);
        }

        function getBranches() {
            return $http.get('/Branch/GetBranches?id=' + service.convId);
        }

        function getBranch(id) {
            return $http.get('/Branch/GetBranch?id=' + id);
        }

        function getMessages(parameters) {
            var branch = '';
            api.convId = convId;

            if (service.branchId != null) {
                branch = '&branchid=' + service.branchId;
            } else {
            }
            return $http.get('/Message/GetMessages?convid=' + service.convId + branch);
        }

        function sendMessage(message) {
            var t = JSON.stringify(message);
            $http.post('/Message/AddMessage', message)
            .success(function (data) {
                console.log('ok', data);
            })
            .error(function (data, status, header, config) {
                console.log('fail', status, header, config);
            });
        }

        function addBranch(branch) {
            $http.post('/Branch/AddBranch', branch)
            .success(function (data) {
                console.log('ok', data);
            })
            .error(function (data, status, header, config) {
                console.log('fail', status, header, config);
            });
        }

        function editConversation(conversation) {
            $http.post('/Conversation/EditConversation', conversation)
            .success(function (data) {
                console.log('ok', data);
            })
            .error(function (data, status, header, config) {
                console.log('fail', status, header, config);
            });
        }

        function editBranch(branch) {
            $http.post('/Branch/EditBranch', branch)
            .success(function (data) {
                console.log('ok', data);
            })
            .error(function (data, status, header, config) {
                console.log('fail', status, header, config);
            });
        }
           
        function createConversation(conversation) {
            $http.post('/Conversation/AddConversation', conversation)
            .success(function (data) {
                console.log('ok', data);
            })
            .error(function (data, status, header, config) {
                console.log('fail', status, header, config);
            });
        }
    }
})();