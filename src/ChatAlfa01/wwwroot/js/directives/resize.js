﻿(function() {
    'use strict';

    angular
        .module('app')
        .directive('resize', resize);

    resize.$inject = ['$window'];
    
    function resize ($window) {
        return function (scope, element) {
            var w = angular.element($window);
            scope.getWindowDimensions = function () {
                return {
                    'h': w.height(),
                    'w': w.width()
                };
            };
            scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
                scope.windowHeight = newValue.h;
                scope.windowWidth = newValue.w;

                scope.style = function (offset) {
                    return {
                        'height': (newValue.h - offset) + 'px'
                        //'width': (newValue.w - 100) + 'px'
                    };
                };

                scope.maxHeight = function (offset) {
                    return {
                        'max-height': (newValue.h - offset) + 'px'
                        //'width': (newValue.w - 100) + 'px'
                    };
                };

            }, true);

            w.bind('resize', function () {
                scope.$apply();
            });
        }
    }

})();