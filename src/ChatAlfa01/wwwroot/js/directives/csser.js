﻿(function() {
    'use strict';

    angular
        .module('app')
        .directive('csser', csser);

    csser.$inject = ['$window'];
    
    function csser ($window) {
        return function (scope, element) {                      
                scope.padding = function (l,t,r,b) {
                    return {
                        'padding-left': l + 'px',
                        'padding-top': t + 'px',
                        'padding-right': r + 'px',
                        'padding-bottom': b + 'px'
                    };
                };

                scope.margin = function (l,t,r,b) {
                    return {
                        'margin-left': l + 'px',
                        'margin-top': t + 'px',
                        'margin-right': r + 'px',
                        'margin-bottom': b + 'px'
                    };
                };
               

            }            
        }
})();