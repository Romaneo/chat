﻿using System;
using System.Collections.Generic;

namespace ChatAlfa01.ViewModels.Chat
{
    public class MessageViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public bool HasAttacments { get; set; }

        public string UserIcon { get; set; }
        public string Author { get; set; }
        public int BranchId { get; set; }
        public int ConversationId { get; set; }
        public List<int> AttachmentIds { get; set; }
    }
}
