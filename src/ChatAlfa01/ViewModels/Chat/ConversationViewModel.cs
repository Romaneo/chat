﻿using System.Collections.Generic;

namespace ChatAlfa01.ViewModels.Chat
{
    public class ConversationViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsPublic { get; set; }

        public string CreatorId { get; set; }
        public List<string> Members { get; set; }
        public List<int> BranchIds { get; set; }
    }
}
