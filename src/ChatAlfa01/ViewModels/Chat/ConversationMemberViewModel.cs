﻿namespace ChatAlfa01.ViewModels.Chat
{
    public class ConversationMemberViewModel
    {
        public int Id { get; set; }
        public bool Notify { get; set; }

        public string UserId { get; set; }
        public int ConversationId { get; set; }
        public int BranchId { get; set; }
    }
}
