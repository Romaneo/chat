﻿namespace ChatAlfa01.ViewModels.Chat
{
    public class UserViewModel
    {
        public string Name { get; set; }
        public string ImgPath { get; set; } = "images/user.png";
        public string Id { get; set; }
    }
}
