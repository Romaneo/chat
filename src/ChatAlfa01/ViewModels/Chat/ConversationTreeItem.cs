﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatAlfa01.ViewModels.Chat
{
    public class ConversationTreeItem
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public bool Show { get; set; } = false;
        public bool IsConv { get; set; } = true;
        public List<BranchTreeItem> Branches { get; set; }
    }

    public class BranchTreeItem
    {
        public BranchTreeItem()
        {
            Branches = new List<BranchTreeItem>();
        }

        public string Name { get; set; }
        public int Id { get; set; }
        public int ConvId { get; set; }
        public bool Show { get; set; } = false;
        public List<BranchTreeItem> Branches { get; set; }
    }
}
