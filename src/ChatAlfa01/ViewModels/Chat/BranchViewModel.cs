﻿using System.Collections.Generic;

namespace ChatAlfa01.ViewModels.Chat
{
    public class BranchViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsMain { get; set; }

        public int ParentId { get; set; }
        public string Path { get; set; }
        public int Level { get; set; }

        public int ConvId { get; set; }
        public List<string> Members { get; set; }
        public List<int> MessagesId { get; set; }
    }
}
