﻿namespace ChatAlfa01.ViewModels.Chat
{
    public class AttachmentViewModel
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public string Hash { get; set; }
        public string Name { get; set; }

        public int MessageId { get; set; }
    }
}
